const $ = document.querySelector.bind(document);
const $$ = document.querySelectorAll.bind(document);

const PLAYER_STORAGE_KEY = "F8_PLAYER";

const player = $(".player");
const cd = $(".cd");
const heading = $("header h2");
const cdThumb = $(".cd-thumb");
const audio = $("#audio");
const playBtn = $(".btn-toggle-play");
const progress = $("#progress");
const nextBtn = $(".btn-next");
const prevBtn = $(".btn-prev");
const randomBtn = $(".btn-random");
const repeatBtn = $(".btn-repeat");
const playlist = $(".playlist");

const app = {
  currentIndex: 0,
  isPlaying: false,
  isRandom: false,
  isRepeat: false,
  config: JSON.parse(localStorage.getItem(PLAYER_STORAGE_KEY)) || {},
  songs: [
    {
      name: "Nắng Ấm Xa Dần",
      singer: "Sơn Tùng MTP",
      path: "../../assets/music/NangAmXaDanLofiVersion-SonTungMTP-6995462.mp3",
      image: "../../assets/images/st.jpg",
    },
    {
      name: "Anh Ghét Làm Bạn Em",
      singer: "Phan Mạnh Quỳnh",
      path: "../../assets/music/Anh Ghet Lam Ban Em - Phan Manh Quynh.mp3",
      image: "../../assets/images/pmq.jpg",
    },
    {
      name: "Bay Thật Xa",
      singer: "Wowwy",
      path: "../../assets/music/Bay That Xa - Wowy_ Karik.mp3",
      image: "../../assets/images/www.jpg",
    },
    {
      name: "Cô Đơn Trong Nhà Mình",
      singer: "Hoài Lâm",
      path: "../../assets/music/Co Don Trong Nha Minh - Hoai Lam.mp3",
      image: "../../assets/images/hl.jpg",
    },
    {
      name: "Cung Đàn Vỡ Đôi",
      singer: "Chipu",
      path: "../../assets/music/Cung dan vo doi_ - Chi Pu.mp3",
      image: "../../assets/images/chipu.jpg",
    },
    {
      name: "Dấu Mưa",
      singer: "Trung Quân Idol",
      path: "../../assets/music/Dau Mua - Trung Quan.mp3",
      image: "../../assets/images/mmg0488_2_yeka.png",
    },
    {
      name: "The Playahh",
      singer: "Soobin Hoàng Sơn",
      path: "../../assets/music/Thang Nam THE PLAYAH_ - Soobin Hoang Son.mp3",
      image: "../../assets/images/sb.jpg",
    },
    {
      name: "Thương",
      singer: "Karik",
      path: "../../assets/music/Thuong - Karik_ Uyen Pim.mp3",
      image: "../../assets/images/kr.jpg",
    },
    {
      name: "Về Bên Anh",
      singer: "Jack",
      path: "../../assets/music/Ve Ben Anh - Jack G5R_ - Jack_ J97.mp3",
      image: "../../assets/images/jack.jpg",
    },
    {
      name: "Yêu Không Nghỉ Phép",
      singer: "Issac",
      path: "../../assets/music/Yeu-Khong-Nghi-Phep-Isaac-OnlyC-DJ-Gin.mp3",
      image: "../../assets/images/issac.jpg",
    },
  ],

  setConfig: function (key, value) {
    this.config[key] = value;
    localStorage.setItem(PLAYER_STORAGE_KEY, JSON.stringify(this.config));
  },

  render: function () {
    const htmls = this.songs.map((song, index) => {
      return `
        <div class="song ${
          index === this.currentIndex ? "active" : ""
        }" data-index = "${index}">
        <div
          class="thumb"
          style="
            background-image: url('${song.image}');
          "
        ></div>
        <div class="body">
          <h3 class="title">${song.name}</h3>
          <p class="author">${song.singer}</p>
        </div>
        <div class="option">
          <i class="fas fa-ellipsis-h"></i>
        </div>
      </div>
        `;
    });
    playlist.innerHTML = htmls.join("");
  },

  defineProperties: function () {
    Object.defineProperty(this, "currentSong", {
      get: function () {
        return this.songs[this.currentIndex];
      },
    });
  },

  handleEvent: function () {
    const _this = this;
    const cdWidth = cd.offsetWidth;

    // XỬ LÝ QUAY CD
    const cdThumbAnimate = cdThumb.animate([{ transform: "rotate(360deg)" }], {
      duration: 10000,
      iterations: Infinity,
    });
    cdThumbAnimate.pause();

    // XỬ LÝ PHÓNG TO THU NHỎ CD
    document.onscroll = function () {
      const scrollTop = window.screenY || document.documentElement.scrollTop;
      const newCdWidth = cdWidth - scrollTop;
      cd.style.width = newCdWidth > 0 ? newCdWidth + "px" : 0;
      cd.style.opacity = newCdWidth / cdWidth;
    };

    // XỬ LÝ KHI CLICK PLAY
    playBtn.onclick = function () {
      if (_this.isPlaying) {
        audio.pause();
      } else {
        audio.play();
      }
    };

    // KHI SONG ĐƯỢC PLAY
    audio.onplay = function () {
      _this.isPlaying = true;
      player.classList.add("playing");
      cdThumbAnimate.play();
    };

    // KHI SONG ĐƯỢC PAUSE
    audio.onpause = function () {
      _this.isPlaying = false;
      player.classList.remove("playing");
      cdThumbAnimate.pause();
    };

    // KHI TIẾN ĐỘ BÀI HÁT THAY ĐỔI
    audio.ontimeupdate = function () {
      if (audio.duration) {
        const progressPercent = Math.floor(
          (audio.currentTime / audio.duration) * 100
        );
        progress.value = progressPercent;
      }
    };

    // XỬ LÝ KHI TUA BÀI HÁT
    progress.onchange = function (e) {
      const seekTime = (audio.duration / 100) * e.target.value;
      audio.currentTime = seekTime;
    };

    // KHI NEXT BÀI HÁT
    nextBtn.onclick = function () {
      if (_this.isRandom) {
        _this.playRandomSong();
      } else {
        _this.nextSong();
      }
      audio.play();
      _this.render();
      _this.scrollToActiveSong();
    };

    // KHI PREV BÀI HÁT
    prevBtn.onclick = function () {
      if (_this.isRandom) {
        _this.playRandomSong();
      } else {
        _this.prevSong();
      }
      audio.play();
      _this.render();
      _this.scrollToActiveSong();
    };

    // KHI RANDOM BÀI HÁT
    randomBtn.onclick = function (e) {
      _this.isRandom = !_this.isRandom;
      _this.setConfig("isRandom", _this.isRandom);
      randomBtn.classList.toggle("active", _this.isRandom);
    };

    // XỬ LÝ PHÁT LẠI BÀI HÁT
    repeatBtn.onclick = function (e) {
      _this.isRepeat = !_this.isRepeat;
      _this.setConfig("isRepeat", _this.isRepeat);
      repeatBtn.classList.toggle("active", _this.isRepeat);
    };

    // XỬ LÝ NEXT SONG KHI HẾT BÀI
    audio.onended = function () {
      if (_this.isRepeat) {
        audio.play();
      } else {
        nextBtn.click();
      }
    };

    // LẮNG NGHE HÀNH VI CLICK VÀO PLAYLIST
    playlist.onclick = function (e) {
      const songNode = e.target.closest(".song:not(.active)");

      if (songNode || !e.target.closest(".option")) {
        // XỬ LÝ KHI CLICK VÀO SONG
        if (songNode) {
          _this.currentIndex = Number(songNode.dataset.index);
          _this.loadCurrentSong();
          _this.render();
          audio.play();
        }
        //XỬ LÝ KHI CLICK VÀO SONG OPTION
        if (e.target.closet(".option")) {
        }
      }
    };
  },

  loadCurrentSong: function () {
    heading.textContent = this.currentSong.name;
    cdThumb.style.backgroundImage = `url('${this.currentSong.image}')`;
    audio.src = this.currentSong.path;
  },

  loadConfig: function () {
    this.isRandom = this.config.isRandom;
    this.isRepeat = this.config.isRepeat;
  },

  nextSong: function () {
    this.currentIndex++;
    if (this.currentIndex >= this.songs.length) {
      this.currentIndex = 0;
    }
    this.loadCurrentSong();
  },

  prevSong: function () {
    this.currentIndex--;
    if (this.currentIndex < 0) {
      this.currentIndex = this.songs.length - 1;
    }
    this.loadCurrentSong();
  },

  playRandomSong: function () {
    let newIndex;
    do {
      newIndex = Math.floor(Math.random() * this.songs.length);
    } while (newIndex === this.currentIndex);
    this.currentIndex = newIndex;
    this.loadCurrentSong();
  },

  scrollToActiveSong: function () {
    setTimeout(() => {
      $(".song.active").scrollIntoView(
        {
          behavior: "smooth",
          block: "nearest",
        },
        300
      );
    });
  },

  start: function () {
    // GÁN CẤU HÌNH  TỪ CONFIG VÀO ỨNG DỤNG
    this.loadConfig();

    // ĐỊNH NGHĨA CÁC THUỘC TÍNH CHO OBJECT
    this.defineProperties();

    // LẮNG NGHE VÀ XỬ LÝ CÁC SỰ KIỆN DOM EVENTS
    this.handleEvent();

    // TẢI THÔNG TIN BÀI HÁT ĐẦU TIÊN VÀO UI KHI CHẠY ỨNG DỤNG
    this.loadCurrentSong();

    // RENDER LẠI PLAYLIST
    this.render();

    // HIỂN THỊ TRẠNG THÁI BAN ĐẦU CỦA BUTTON REPEAT VÀ RANDOM
    randomBtn.classList.toggle("active", this.isRandom);
    repeatBtn.classList.toggle("active", this.isRepeat);
  },
};

app.start();
